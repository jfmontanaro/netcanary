#!/usr/local/bin/python3
import re
import subprocess
import time
import traceback
import boto3


regex = re.compile(r'time=(\d+\.\d+)')
def check_latency(address, timeout=10):
    try:
        p = subprocess.run(
            ['/bin/ping', '-c 1', address],
            timeout=timeout,
            stdout=subprocess.PIPE,
            shell=False
        )
        match = regex.search(p.stdout.decode('ascii'))
        if match:
            return 1 # float(match.groups()[0])
        else:
            return 0 # -1
    except subprocess.TimeoutExpired:
        return 0 # -1


start = time.monotonic()
period = 60
cloudwatch = boto3.client('cloudwatch')
# time.sleep(3)
print('Starting up...')
while True:
    try:
        c_online = check_latency('192.168.30.208')
        metric_data = [
            {
                'MetricName': 'NetworkOnline',
                'Dimensions': [{
                    'Name': 'Building',
                    'Value': 'Bryant C'
                }],
                'Value': c_online
            },
            {
                'MetricName': 'NetworkOnline',
                'Dimensions': [{
                    'Name': 'Building',
                    'Value': 'Bryant E'
                }],
                'Value': 1
            }
        ]
        cloudwatch.put_metric_data(Namespace='MODG Meatspace', MetricData=metric_data)
    except Exception:
        traceback.print_exc()
    time.sleep(period - ((time.monotonic() - start) % period))

FROM python:3-slim

RUN pip3 install boto3

RUN apt-get update
RUN apt-get install -y inetutils-ping

COPY ./netcanary.py /usr/bin/netcanary
RUN chmod +x /usr/bin/netcanary

CMD ["netcanary"]
